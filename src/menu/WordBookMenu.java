package menu;

import dto.WordBookType;
import dto.WordDto;
import service.WordBookService;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Scanner;

public class WordBookMenu implements Menu{
    private Scanner sc;
    private final WordBookService wordBookService;

    public WordBookMenu(WordBookService wordBookService) {
        sc = new Scanner(System.in, StandardCharsets.UTF_8);
        this.wordBookService = wordBookService;
    }


    public void wordBookStart(WordBookType wordBookType) {

        System.out.print("\nВведите команду (для просмотра списка команд введите help): ");
        sc = new Scanner(System.in, StandardCharsets.UTF_8);
        String command = sc.nextLine().trim();
        while (!command.equals("quit")) {
            switch (command) {
                case "help":
                    System.out.println("read - прочитать содержимое словаря\n" +
                            "find - найти перевод слова\n" +
                            "add - добавить слово в словарик\n" +
                            "update - изменить перевод слова\n" +
                            "delete - удалить перевод слова\n" +
                            "quit - выйти из программы.");
                    break;
                case "find":
                    findByKey(wordBookType);
                    break;
                case "read":
                    findAll(wordBookType);
                    break;
                case "add":
                    addWord(wordBookType);
                    break;
                case "update":
                    updateWord(wordBookType);
                    break;
                case "delete":
                    deleteWord(wordBookType);
                    break;
                case "quit":
                    stop();
                    break;
                default:
                    error();
                    break;
            }
            wordBookStart(wordBookType);
        }
        stop();
    }

    private void findByKey(WordBookType wordBookType) {
        System.out.print("Введите слово: ");
        String key = sc.nextLine();

        WordDto word = wordBookService.findWordByKey(key, wordBookType);
        if (!word.getKey().isEmpty()) {
            System.out.println("\n\t" + word);
        }
    }

    private void deleteWord(WordBookType wordBookType) {
        System.out.print("Введите слово, которое нужно удалить: ");
        String key = sc.nextLine();
        wordBookService.deleteWord(key, wordBookType);
    }

    private void updateWord(WordBookType wordBookType) {
        System.out.print("Введите слово и новый перевод для него: ");
        String key = sc.next();
        String newValue = sc.next();
        WordDto word = new WordDto(key, newValue, wordBookType);
        wordBookService.updateWord(word);
    }

    private void addWord(WordBookType wordBookType) {
        System.out.print("Введите слово и его перевод для добавления (через пробел): ");
        String key = sc.next();
        String value = sc.next();
        wordBookService.addNewWord(new WordDto(key, value, wordBookType));
    }

    private void findAll(WordBookType wordBookType) {
        System.out.println("Содержимое словаря:\n");
        List<WordDto> words = wordBookService.readAllWords(wordBookType);
        for (WordDto word :
                words) {
            System.out.printf("\t%s: %s\n", word.getKey(), word.getValue());
        }
    }

    public void stop() {
        System.exit(0);
    }

    public void error() {
        System.out.println("\nВведённый вами текст не является командой. Попробуйте ещё раз!");
    }
}