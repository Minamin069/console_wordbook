package menu;

import dto.WordBookType;

public interface Menu {
    public void wordBookStart(WordBookType wordBookType);

    public void stop();

    public void error();
}
