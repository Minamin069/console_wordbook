package utils;

public interface Validation {
    public boolean validate(String string);
    public String getRequirements();
}
