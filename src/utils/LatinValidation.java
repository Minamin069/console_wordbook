package utils;

public class LatinValidation implements Validation {
    private String requirements = "Длина слова может быть только 4 символа и только буквы латинской раскладки.";
    public boolean validate(String string) {
        return string.length() == 4 && string.matches("[a-zA-Z]+");
    }

    public String getRequirements() {
        return requirements;
    }
}
