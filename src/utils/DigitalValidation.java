package utils;

public class DigitalValidation implements Validation {
    private String requirements = "Длина слова может быть только 5 символов, и эти символы - только цифры.";
    public boolean validate(String string) {
        return string.length() == 5 && string.matches("\\d+");
    }

    public String getRequirements() {
        return requirements;
    }
}
