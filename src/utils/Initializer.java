package utils;

import menu.WordBookMenu;
import dto.WordBookType;
import repository.DigWordBookRepository;
import repository.LatWordBookRepository;
import repository.WordRepository;
import service.WordBookService;
import service.WordBookServiceImpl;

public class Initializer {
    private WordRepository wordBookRepository;

    private final WordBookMenu wordBookMenu;
    private Validation validation;

    public Initializer(WordBookType wordBookType) {

        if (wordBookType == WordBookType.LATIN) {
            wordBookRepository = new LatWordBookRepository();
            validation = new LatinValidation();
        } else if (wordBookType == WordBookType.DIGITAL) {
            wordBookRepository = new DigWordBookRepository();
            validation = new DigitalValidation();
        }

        WordBookService wordBookService = new WordBookServiceImpl(wordBookRepository, validation);
        wordBookMenu = new WordBookMenu(wordBookService);
    }

    public WordBookMenu getWordBookController() {
        return wordBookMenu;
    }
}
