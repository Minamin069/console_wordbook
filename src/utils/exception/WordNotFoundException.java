package utils.exception;

import utils.Validation;

public class WordNotFoundException extends Exception{
    public WordNotFoundException(String message) {
        super(message);
    }
    public WordNotFoundException(String message, Validation validator) {
        super(message + "\n" + validator.getRequirements());
    }
}
