package repository;

import dto.WordBookType;
import dto.WordDto;
import utils.exception.WordNotFoundException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LatWordBookRepository implements WordRepository{

    public void addWord(WordDto word) throws IOException {
        File file = getFile();
        FileWriter fileWriter = new FileWriter(file, true);
        fileWriter.append(word.getKey() + ": " + word.getValue() + "\n");
        fileWriter.close();
    }

    public WordDto findWordByKey(String key, WordBookType wordBookType) throws IOException, WordNotFoundException {
        File file = getFile();

        Scanner sc = new Scanner(file);
        while (sc.hasNext()) {
            String readKey = sc.next();
            String readValue = sc.next();
            if (readKey.substring(0, readKey.length() - 1).equals(key)) {
                return new WordDto(key, readValue, wordBookType);
            }
        }
        throw new WordNotFoundException(String.format("Слово %s не найдено!", key));
    }

    public void deleteWord(String key, WordBookType wordBookType) throws IOException, WordNotFoundException {
        String result = "";
        boolean wordExist = false;
        File file = getFile();
        Scanner sc = new Scanner(file);
        while (sc.hasNext()) {
            String readKey = sc.next();
            String readValue = sc.next();
            if (readKey.substring(0, readKey.length() - 1).equals(key)) {
                wordExist = true;
                continue;
            }
            result += readKey + " " + readValue + "\n";
        }
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.append(result);
        fileWriter.close();
        if (!wordExist) throw new WordNotFoundException("Слово для удаления не найдено!");
    }

    public void updateWord(WordDto word) throws IOException, WordNotFoundException {
        String result = "";
        boolean wordExist = false;
        File file = getFile();
        Scanner sc = new Scanner(file);
        while (sc.hasNext()) {
            String readKey = sc.next();
            String readValue = sc.next();
            if (readKey.substring(0, readKey.length() - 1).equals(word.getKey())) {
                readValue = word.getValue();
                wordExist = true;
            }
            result += readKey + " " + readValue + "\n";
        }
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.append(result);
        fileWriter.close();
        if (!wordExist) throw new WordNotFoundException("Слово для обновления не найдено!");
    }

    public List<WordDto> readAllWords(WordBookType wordBookType) throws IOException {
        List<WordDto> result = new ArrayList<>();
        File file = getFile();
        Scanner sc = new Scanner(file);
        while (sc.hasNext()) {
            String readKey = sc.next();
            String readValue = sc.next();
            result.add(new WordDto
                    (
                            readKey.substring(0, readKey.length() - 1),
                            readValue,
                            wordBookType
                    )
            );
        }
        return result;
    }

    private File getFile() throws IOException {
        File file =  new File("latWordBook.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;
    }
}
