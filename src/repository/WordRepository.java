package repository;

import dto.WordBookType;
import dto.WordDto;
import utils.exception.WordNotFoundException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface WordRepository {
    public void addWord(WordDto word) throws IOException;

    public WordDto findWordByKey(String key, WordBookType wordBookType) throws IOException, WordNotFoundException;

    public void deleteWord(String key, WordBookType wordBookType) throws IOException, WordNotFoundException;

    public void updateWord(WordDto word) throws IOException, WordNotFoundException;

    public List<WordDto> readAllWords(WordBookType wordBookType) throws IOException;
}