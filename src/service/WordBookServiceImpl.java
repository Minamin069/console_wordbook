package service;

import dto.WordBookType;
import dto.WordDto;
import repository.WordRepository;
import utils.Validation;
import utils.exception.WordNotFoundException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class WordBookServiceImpl implements WordBookService {
    private final WordRepository wordRepository;
    private final Validation validator;

    public WordBookServiceImpl(WordRepository wordRepository, Validation validator) {
        this.wordRepository = wordRepository;
        this.validator = validator;
    }

    public WordDto findWordByKey(String key, WordBookType wordBookType) {
        try {
            return wordRepository.findWordByKey(key, wordBookType);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new WordDto("", "", wordBookType);
        }
    }

    public List<WordDto> readAllWords(WordBookType wordBookType) {
        try {
            return wordRepository.readAllWords(wordBookType);
        } catch (IOException e) {
            return null;
        }

    }

    public void deleteWord(String key, WordBookType wordBookType) {
        try {
            wordRepository.deleteWord(key, wordBookType);
            System.out.println("Слово успешно удалено!");
        }  catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void updateWord(WordDto word) {
        try {
            if (validator.validate(word.getKey())) {
                wordRepository.updateWord(word);
                System.out.println("Слово успешно изменено!");
            }
            else {
                throw new WordNotFoundException("Слово не соответствует требованиям словаря!", validator);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void addNewWord(WordDto word) {
        try {
            if (validator.validate(word.getKey())) {
                wordRepository.addWord(word);
                System.out.println("Слово успешно добавлено!");
            }
            else {
                throw new WordNotFoundException("Слово не соответствует требованиям словаря!", validator);
            }
        } catch (IOException e) {
            System.out.println("Файл не найден!");
        } catch (WordNotFoundException w) {
            System.out.println(w.getMessage());
        }
    }
}