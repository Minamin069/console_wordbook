package service;

import dto.WordBookType;
import dto.WordDto;
import utils.exception.WordNotFoundException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface WordBookService {
    public WordDto findWordByKey(String key, WordBookType wordBookType);

    public List<WordDto> readAllWords(WordBookType wordBookType);

    public void deleteWord(String key, WordBookType wordBookType);

    public void updateWord(WordDto word);

    public void addNewWord(WordDto wordDto);
}
