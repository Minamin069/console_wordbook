import menu.WordBookMenu;
import dto.WordBookType;
import utils.Initializer;

import java.util.Scanner;


public class Main {
    static WordBookType wordBookType;

    public static void main(String[] args) {
        Initializer init = new Initializer(chooseWBType());
        WordBookMenu wordBookMenu = init.getWordBookController();
        wordBookMenu.wordBookStart(wordBookType);
    }

    public static WordBookType chooseWBType() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Добро пожаловать в словарик! Введите номер словаря\n" +
                " (1 - латинские буквы, 2 - цифры): ");
        String workBookTypeStr = sc.next();
        if (!workBookTypeStr.equals("quit")) {
            while (wordBookType == null) {
                switch (workBookTypeStr) {
                    case "1":
                        wordBookType = WordBookType.LATIN;
                        break;
                    case "2":
                        wordBookType = WordBookType.DIGITAL;
                        break;
                    default:
                        System.out.println("Нет словаря с номером " + workBookTypeStr + "!\n" +
                                "Попробуйте ещё раз! ");
                        workBookTypeStr = sc.next();
                }
            }
        }
        return wordBookType;
    }
}
