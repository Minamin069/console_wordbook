package dto;

public enum WordBookType {
    LATIN, DIGITAL
}
