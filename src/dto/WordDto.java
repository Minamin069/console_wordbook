package dto;

public class WordDto {
    String key;
    String value;
    WordBookType wordBookType;

    public WordDto(String key, String value, WordBookType wordBookType) {
        this.key = key;
        this.value = value;
        this.wordBookType = wordBookType;
    }

    @Override
    public String toString() {
        return key + ": " + value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public WordBookType getWordBookType() {
        return wordBookType;
    }
}
